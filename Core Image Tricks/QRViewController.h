//
//  QRViewController.h
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/14/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

@import UIKit;
#import "CIView.h"

@interface QRViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet CIView *qrView;
@property (nonatomic, strong) IBOutlet UITextField *textField;

@end
