//
//  FilteredViewController.m
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/4/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import "FilteredViewController.h"


@interface FilteredViewController () {
    UIBarButtonItem *resetButton;
}

-(void)reset;

@end

@implementation FilteredViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    resetButton = [[UIBarButtonItem alloc] initWithTitle:@"Reset" style:UIBarButtonItemStylePlain target:self action:@selector(reset)];
    resetButton.enabled = NO;
    self.navigationItem.rightBarButtonItem = resetButton;
    
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width*4, self.view.frame.size.height);
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.msnbc.com"]]];
}

-(void)viewWillDisappear:(BOOL)animated {
    [self.filteredView reset];
}

-(void)blur:(UIButton *)sender {
    [self.filteredView blur];
    resetButton.enabled = YES;
}

-(void)pixellate:(UIButton *)sender {
    [self.filteredView pixellate];
    resetButton.enabled = YES;
}

-(void)invert:(UIButton *)sender {
    [self.filteredView invert];
    resetButton.enabled = YES;
}

-(void)desaturate:(UIButton *)sender {
    [self.filteredView desaturate];
    resetButton.enabled = YES;
}

-(void)tint:(UIButton *)sender {
    [self.filteredView tint];
    resetButton.enabled = YES;
}

-(void)reset {
    [self.filteredView reset];
}

@end
