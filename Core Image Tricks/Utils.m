//
//  Utils.m
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/4/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(AppDelegate *)appDelegate {
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

+(NSArray *)constraintsForViews:(NSDictionary *)views withVisualFormats:(NSString *)firstFormat, ... {
    NSMutableArray *constraints = [NSMutableArray array];
    
    va_list args;
    va_start(args, firstFormat);
    NSString * formatString = firstFormat;
    while (formatString != nil) {
        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:formatString options:0 metrics:nil views:views]];
        formatString = va_arg(args, NSString *);
    }
    va_end(args);
    
    return constraints;
}

@end
