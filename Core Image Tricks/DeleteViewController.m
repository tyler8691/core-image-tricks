//
//  DeleteViewController.m
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/21/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import "DeleteViewController.h"

@interface DeleteViewController () {
    NSInteger cellCount;
    NSArray *contactNames, *contactPhones, *contactImages;
}

-(NSString *)pickRandomFromArray:(NSArray *)array;

@end

@implementation DeleteViewController

static NSString *CellIdentifier = @"Cell";

-(id)init {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(140.0, 135.0);
    layout.minimumInteritemSpacing = 10.0;
    layout.minimumLineSpacing = 10.0;
    
    self = [super initWithCollectionViewLayout:layout];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Tap To Delete";
    
    self.collectionView.contentInset = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
    
    cellCount = 50;
    
    [self.collectionView registerClass:[DeleteCell class] forCellWithReuseIdentifier:CellIdentifier];
    
    //Randomly generated names
    contactNames = @[@"Broderick Lomax",
                     @"Vicenta Baines",
                     @"Eileen Weinmann",
                     @"Nicola Gum",
                     @"Rebekah Burchett",
                     @"Lianne Castello",
                     @"Kellee Roca",
                     @"Tania Vrabel",
                     @"Nadene Trybus",
                     @"Ethel Beddingfield",
                     @"Tabatha Sorge",
                     @"Ruth Petite",
                     @"Danuta Canterbury",
                     @"Delmar Urick",
                     @"Collin Stabile",
                     @"Jessica Brandes",
                     @"Aura Goodpasture",
                     @"Sirena Vine",
                     @"Leeanna Steele",
                     @"Francie Tomei"];
    
    //Randomly generated phone numbers
    contactPhones = @[@"(804)123-2323",
                      @"(223)934-9845",
                      @"(223)094-4569",
                      @"(347)698-2347",
                      @"(345)776-3993",
                      @"(577)349-5688"];
    
    //Random contact images
    contactImages = @[@"contact_image_1",
                      @"contact_image_2",
                      @"contact_image_3"];

}


#pragma mark - Collection view data source

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return cellCount;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DeleteCell *cell = (DeleteCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    cell.contactImageView.image = [UIImage imageNamed:[self pickRandomFromArray:contactImages]];
    cell.nameLabel.text = [self pickRandomFromArray:contactNames];
    cell.phoneLabel.text = [self pickRandomFromArray:contactPhones];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    DeleteCell *cell = (DeleteCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell animateDeletion];
}

#pragma mark - Delete Cell Delegate

-(void)deleteAnimationCompleted:(DeleteCell *)cell {
    NSIndexPath *path = [self.collectionView indexPathForCell:cell];
    if (path) {
        cellCount--;
        [self.collectionView deleteItemsAtIndexPaths:@[path]];
    }
}

#pragma mark - Helper Methods

-(NSString *)pickRandomFromArray:(NSArray *)array {
    if (array && array.count > 0) return [array objectAtIndex:arc4random_uniform((uint32_t)array.count)];
    else return @"";
}

@end
