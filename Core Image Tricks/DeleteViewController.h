//
//  DeleteViewController.h
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/21/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeleteCell.h"

@interface DeleteViewController : UICollectionViewController <DeleteCellDelegate>

@end
