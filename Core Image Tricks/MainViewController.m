//
//  MainViewController.m
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/4/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import "MainViewController.h"
#import "FilterViewController.h"
#import "FilteredViewController.h"
#import "QRViewController.h"
#import "DeleteViewController.h"

#import "CIView.h"
@import CoreImage;

@interface MainViewController () {
    NSArray *cellTitles;
    CIView *menuOverlay;
}

@end

@implementation MainViewController

static NSString *CellIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:[Utils appDelegate] action:@selector(toggleMenu)];
    
    cellTitles = @[@"Basic Image Filter", @"Real-Time Filter", @"QR Code Generator", @"Delete Animation"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return cellTitles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = cellTitles[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

#pragma mark - Table View Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0: {
            FilterViewController *vc = [[FilterViewController alloc] initWithNibName:@"FilterViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 1: {
            FilteredViewController *vc = [[FilteredViewController alloc] initWithNibName:@"FilteredViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 2: {
            QRViewController *vc = [[QRViewController alloc] initWithNibName:@"QRViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 3: {
            DeleteViewController *vc = [[DeleteViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        default:
            break;
    }
}

@end
