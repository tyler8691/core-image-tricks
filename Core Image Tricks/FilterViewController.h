//
//  FilterViewController.h
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/4/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

@import UIKit;
#import "CIView.h"

@interface FilterViewController : UIViewController <UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet CIView *filterView;

@end
