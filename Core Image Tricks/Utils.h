//
//  Utils.h
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/4/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

@import Foundation;
#import "AppDelegate.h"

@interface Utils : NSObject

+(AppDelegate *)appDelegate;

+(NSArray *)constraintsForViews:(NSDictionary *)views withVisualFormats:(NSString *)firstFormat, ... NS_REQUIRES_NIL_TERMINATION;

@end
