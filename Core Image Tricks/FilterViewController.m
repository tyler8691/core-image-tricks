//
//  FilterViewController.m
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/4/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import "FilterViewController.h"

@interface FilterViewController ()

-(void)editFilter;

@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CIImage *flowers = [[CIImage alloc] initWithImage:[UIImage imageNamed:@"flowers.jpeg"]];
    
    self.filterView.sourceImage = flowers;
    [self.filterView addFilterNamed:@"CISepiaTone" withKeysAndValues:nil];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editFilter)];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.filterView render];
}

-(void)editFilter {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Filter" message:@"Enter a filter name (e.g. CIPixellate) to apply with default values." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Apply", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex != [alertView cancelButtonIndex]) {
        NSString *filterValue = [alertView textFieldAtIndex:0].text;
        [self.filterView clearFilters];
        [self.filterView addFilterNamed:filterValue withKeysAndValues:nil];
        [self.filterView render];
    }
}

@end
