//
//  QRViewController.m
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/14/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import "QRViewController.h"

@interface QRViewController ()

@end

@implementation QRViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.qrView addFilterNamed:@"CIQRCodeGenerator" withKeysAndValues:@{@"inputMessage" : [self.textField.text dataUsingEncoding:NSISOLatin1StringEncoding]}];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.qrView render];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [[self.qrView filterNamed:@"CIQRCodeGenerator"] setValue:[newString dataUsingEncoding:NSISOLatin1StringEncoding] forKey:@"inputMessage"];
    [self.qrView render];
    return YES;
}

@end
