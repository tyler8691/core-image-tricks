//
//  DeleteCell.h
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/21/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DeleteCell;

@protocol DeleteCellDelegate <NSObject>

-(void)deleteAnimationCompleted:(DeleteCell *)cell;

@end

@interface DeleteCell : UICollectionViewCell

@property (unsafe_unretained) NSObject<DeleteCellDelegate> *delegate;
@property (nonatomic, strong) UIImageView *contactImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *phoneLabel;


-(void)animateDeletion;

@end
