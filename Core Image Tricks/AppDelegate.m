//
//  AppDelegate.m
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/4/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import "AppDelegate.h"
#import "CIView.h"

@import CoreImage;

@interface AppDelegate() {
    UITableView *sideMenu;
    BOOL sideMenuShowing;
    CGRect originalFrame;
    CIView *overlayView;
    UITapGestureRecognizer *tap;
    NSArray *menuItems;
    CIImage *dotScreenImage, *colorMask;
}

@end

@implementation AppDelegate

static NSString *CellIdentifier = @"Cell";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    menuItems = @[@"Dot Screen Mask", @"Pixellate", @"Darken"];
    
    sideMenu = [[UITableView alloc] initWithFrame:self.window.bounds style:UITableViewStylePlain];
    sideMenu.contentInset = UIEdgeInsetsMake(application.statusBarFrame.size.height, 0.0, 0.0, 0.0);
    sideMenu.delegate = self;
    sideMenu.dataSource = self;
    sideMenu.backgroundColor = [UIColor blackColor];
    sideMenu.separatorStyle = UITableViewCellSeparatorStyleNone;
    [sideMenu registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    [self.window addSubview:sideMenu];
    [self.window sendSubviewToBack:sideMenu];
    
    overlayView = [[CIView alloc] initWithFrame:self.window.bounds];
    overlayView.alpha = 0.0;
    overlayView.sourceView = self.navController.view;
    [self.window addSubview:overlayView];
    [self.window bringSubviewToFront:overlayView];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleMenu)];
    tap.enabled = NO;
    [self.navController.view addGestureRecognizer:tap];
    
    originalFrame = self.navController.view.frame;
    
    //Preload the dot screen mask image
    CIFilter *gradient = [CIFilter filterWithName:@"CISmoothLinearGradient"];
    [gradient setValue:[CIVector vectorWithX:0.0 Y:0.0] forKey:@"inputPoint0"];
    [gradient setValue:[CIVector vectorWithX:280.0 Y:0.0] forKey:@"inputPoint1"];
    [gradient setValue:[CIColor colorWithRed:0.0 green:0.0 blue:0.0] forKey:@"inputColor0"];
    [gradient setValue:[CIColor colorWithRed:0.8 green:0.8 blue:0.8] forKey:@"inputColor1"];
    
    CIFilter *dotScreen = [CIFilter filterWithName:@"CIDotScreen"];
    [dotScreen setValue:[gradient outputImage] forKey:kCIInputImageKey];
    dotScreenImage = [dotScreen outputImage];
    
    //Preload the color mask (for reducing opacity)
    CIFilter *color = [CIFilter filterWithName:@"CIConstantColorGenerator"];
    [color setValue:[CIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4] forKey:@"inputColor"];
    colorMask = [color outputImage];
    
    //Choose dot screen mask by default
    [overlayView addFilterNamed:@"CIBlendWithMask" withKeysAndValues:@{@"inputMaskImage" : dotScreenImage}];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Side Menu

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menuItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = menuItems[indexPath.row];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor blackColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [overlayView clearFilters];
    
    switch (indexPath.row) {
        case 0: {
            [overlayView addFilterNamed:@"CIBlendWithMask" withKeysAndValues:@{@"inputMaskImage" : dotScreenImage}];
            break;
        }
        case 1: {
            CGAffineTransform transform = CGAffineTransformIdentity;
            [overlayView addFilterNamed:@"CIAffineClamp" withKeysAndValues:@{@"inputTransform" : [NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)]}];
            [overlayView addFilterNamed:@"CIPixellate" withKeysAndValues:@{@"inputScale" : @6.0}];
            break;
        }
        case 2: 
            [overlayView addFilterNamed:@"CIColorInvert" withKeysAndValues:nil];
            [overlayView addFilterNamed:@"CIColorControls" withKeysAndValues:@{@"inputSaturation" : @0.0}];
            [overlayView addFilterNamed:@"CIBlendWithMask" withKeysAndValues:@{@"inputMaskImage" : colorMask}];
            break;
        default:
            break;
    }
    
    [overlayView render];
}

-(void)tapNavView {
    [self toggleMenu];
}

-(void)toggleMenu {
    if (!sideMenuShowing) {
        sideMenuShowing = YES;
        
        [overlayView render];
        
        CGRect newFrame = CGRectMake(200.0, self.navController.view.frame.origin.y, self.navController.view.frame.size.width, self.navController.view.frame.size.height);
        
        [UIView animateWithDuration:0.4 animations:^{
            overlayView.frame = newFrame;
            self.navController.view.frame = newFrame;
            overlayView.alpha = 1.0;
        } completion:^(BOOL finished) {
            tap.enabled = YES;
        }];
    } else {
        sideMenuShowing = NO;
        [UIView animateWithDuration:0.4 animations:^{
            self.navController.view.frame = originalFrame;
            overlayView.frame = originalFrame;
            overlayView.alpha = 0.0;
        } completion:^(BOOL finished) {
            tap.enabled = NO;
        }];
    }
}

@end
