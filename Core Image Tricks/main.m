//
//  main.m
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/4/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
