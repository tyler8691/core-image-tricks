//
//  FilteredView.m
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/4/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import "FilteredView.h"
#import "CustomGLKView.h"

@interface FilteredView() {
    CustomGLKView *imageView;
}

-(void)applyFilters;
-(void)setup;

@end

@implementation FilteredView

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

-(void)setup {
    self.backgroundColor = [UIColor whiteColor];
    
    imageView = [[CustomGLKView alloc] initWithFrame:self.bounds];
    imageView.hidden = YES;
    [self addSubview:imageView];
    [self bringSubviewToFront:imageView];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    imageView.frame = self.contentView.frame;
    imageView.sourceView = self.contentView;
}

#pragma mark - Awesome methods

-(void)pixellate {
    [imageView clearFilters];
    
    //Affine clamp prevents dark black border
    CGAffineTransform transform = CGAffineTransformIdentity;
    [imageView addFilterNamed:@"CIAffineClamp" withKeysAndValues:@{@"inputTransform" : [NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)]}];
    [imageView addFilterNamed:@"CIPixellate" withKeysAndValues:@{@"inputScale" : @4.0}];
    [self applyFilters];
}

-(void)blur {
    [imageView clearFilters];
    
    //Affine clamp prevents dark black border
    CGAffineTransform transform = CGAffineTransformIdentity;
    [imageView addFilterNamed:@"CIAffineClamp" withKeysAndValues:@{@"inputTransform" : [NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)]}];
    [imageView addFilterNamed:@"CIGaussianBlur" withKeysAndValues:@{@"inputRadius" : @2.0}];
    [self applyFilters];
}

-(void)invert {
    [imageView clearFilters];
    
    [imageView addFilterNamed:@"CIColorInvert" withKeysAndValues:nil];
    [self applyFilters];
}

-(void)desaturate {
    [imageView clearFilters];
    
    [imageView addFilterNamed:@"CIColorControls" withKeysAndValues:@{@"inputSaturation" : @0.0}];
    [self applyFilters];
}

-(void)tint {
    [imageView clearFilters];
    
    [imageView addFilterNamed:@"CIColorMonochrome" withKeysAndValues:@{@"inputColor" : [CIColor colorWithRed:0.0 green:0.8 blue:0.4],
                                                                       @"inputIntensity" : @0.9}];
    [self applyFilters];
}

#pragma mark - Filter Management

-(void)applyFilters {
    [imageView startRendering];
    imageView.hidden = NO;
}

-(void)reset {
    imageView.hidden = YES;
    [imageView stopRendering];
    [imageView clearFilters];
}

@end
