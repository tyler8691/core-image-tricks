//
//  CIView.m
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/7/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import "CIView.h"
@import CoreImage;

@interface CIView() {
    EAGLContext *context;
    GLuint framebuffer, renderbuffer, texture;
    CADisplayLink *displayLink;
    CIContext *imageContext;
    CGFloat scaleFactor;
    CGRect scaledBounds;
}

-(void)setup;
-(BOOL)checkFramebufferStatus;

@end

@implementation CIView

-(id)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(void)setup {
    _filters = @[];
    
    scaleFactor = [[UIScreen mainScreen] scale];
    
    self.opaque = YES;
    self.userInteractionEnabled = NO;
    self.contentScaleFactor = scaleFactor;
    
    CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
    eaglLayer.opaque = YES;
    eaglLayer.contentsScale = scaleFactor;
    eaglLayer.drawableProperties = @{kEAGLDrawablePropertyRetainedBacking : @NO,
                                     kEAGLDrawablePropertyColorFormat : kEAGLColorFormatRGBA8};
    
    context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!context || ![EAGLContext setCurrentContext:context]) {
        NSLog(@"Unable to initialize EAGLContext");
        return;
    }
    
    imageContext = [CIContext contextWithEAGLContext:context options:@{kCIContextWorkingColorSpace:[NSNull null]}];
    
    [EAGLContext setCurrentContext:context];
    
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    
    glGenRenderbuffers(1, &renderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
    
    [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, renderbuffer);
    
    [self checkFramebufferStatus];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    scaledBounds = CGRectMake(0.0, 0.0, ceilf(self.bounds.size.width) * scaleFactor, ceilf(self.bounds.size.height) * scaleFactor);
    
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
    [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    [self checkFramebufferStatus];
}

-(void)dealloc {
    
    [self stopRendering];
    
    glFlush();
    
    if (framebuffer) {
        glDeleteFramebuffers(1, &framebuffer);
        framebuffer = 0;
    }
    
    if (renderbuffer) {
        glDeleteRenderbuffers(1, &renderbuffer);
        framebuffer = 0;
    }
    
    context = nil;
    imageContext = nil;
}

+(Class)layerClass {
    return [CAEAGLLayer class];
}

-(BOOL)checkFramebufferStatus {
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        NSLog(@"Failed to create framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
        return NO;
    }
    return YES;
}

-(void)startRendering {
    displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(render)];
    displayLink.frameInterval = 2.0;
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

-(void)stopRendering {
    [displayLink invalidate];
}

-(void)render {
    
    [EAGLContext setCurrentContext:context];
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    CIImage *inputImage, *outputImage;
    
    if (self.sourceImage) {
        inputImage = self.sourceImage;
    } else if (self.sourceView) {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, scaleFactor);
        [self.sourceView drawViewHierarchyInRect:self.bounds afterScreenUpdates:NO];
        
        UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
        
        inputImage = [CIImage imageWithCGImage:snapshotImage.CGImage];
        
        UIGraphicsEndImageContext();
    }
    
    if (self.filters && self.filters.count > 0) {
        if (inputImage) [[self.filters objectAtIndex:0] setValue:inputImage forKey:kCIInputImageKey];
        for (int x = 0; x < self.filters.count; x++) {
            CIFilter *filter = [self.filters objectAtIndex:x];
            if (x > 0) [filter setValue:[[self.filters objectAtIndex:x-1] outputImage] forKey:kCIInputImageKey];
            if (x+1 == self.filters.count) outputImage = [filter outputImage];
        }
    } else outputImage = inputImage;
    
#warning fix?
    CGRect outputRect;
    if (!CGRectIsInfinite(outputImage.extent)) outputRect = outputImage.extent;
    else if (!CGRectIsInfinite(inputImage.extent)) outputRect = inputImage.extent;
    else outputRect = scaledBounds;
    
    [imageContext drawImage:outputImage inRect:scaledBounds fromRect:outputRect];
    
    glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
    [context presentRenderbuffer:GL_RENDERBUFFER];
}

#pragma mark - Filter Management

-(CIFilter *)filterNamed:(NSString *)name {
    for (CIFilter *filter in self.filters) {
        if ([filter.name isEqualToString:name]) return filter;
    }
    return nil;
}

-(void)addFilterNamed:(NSString *)filterName withKeysAndValues:(NSDictionary *)dict {
    CIFilter *filter = [self filterNamed:filterName];
    if (filter) return;
    
    filter = [CIFilter filterWithName:filterName];
    if (!filter) return;
    
    for (NSString *key in dict) {
        [filter setValue:[dict objectForKey:key] forKey:key];
    }
    
    _filters = [_filters arrayByAddingObject:filter];
}

-(void)removeFilterNamed:(NSString *)filterName {
    CIFilter *filter = [self filterNamed:filterName];
    if (filter) {
        NSMutableArray *f = [NSMutableArray arrayWithArray:self.filters];
        [f removeObject:filter];
        _filters = [NSArray arrayWithArray:f];
    }
}

-(void)clearFilters {
    _filters = @[];
}

@end
