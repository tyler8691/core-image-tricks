//
//  FilteredView.h
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/4/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

@import UIKit;

@interface FilteredView : UIView

@property (nonatomic, strong) IBOutlet UIView *contentView;

-(void)pixellate;
-(void)blur;
-(void)invert;
-(void)desaturate;
-(void)tint;

-(void)reset;

@end
