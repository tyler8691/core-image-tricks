//
//  FilteredViewController.h
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/4/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

@import UIKit;
#import "FilteredView.h"

@interface FilteredViewController : UIViewController

@property (nonatomic, strong) IBOutlet FilteredView *filteredView;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIWebView *webView;

-(IBAction)blur:(UIButton *)sender;
-(IBAction)pixellate:(UIButton *)sender;
-(IBAction)invert:(UIButton *)sender;
-(IBAction)desaturate:(UIButton *)sender;
-(IBAction)tint:(UIButton *)sender;

@end
