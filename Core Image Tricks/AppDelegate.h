//
//  AppDelegate.h
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/4/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIWindow *window;

@property (nonatomic, strong) IBOutlet UINavigationController *navController;

-(void)toggleMenu;

@end
