//
//  CIView.h
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/7/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

@import GLKit;

@interface CIView : UIView

@property (nonatomic, strong, readonly) NSArray *filters;
@property (nonatomic, assign) UIView *sourceView;
@property (nonatomic, strong) CIImage *sourceImage;

-(void)startRendering;
-(void)render;
-(void)stopRendering;

-(CIFilter *)filterNamed:(NSString *)name;
-(void)addFilterNamed:(NSString *)filterName withKeysAndValues:(NSDictionary *)dict;
-(void)removeFilterNamed:(NSString *)filterName;
-(void)clearFilters;

@end
