//
//  CustomGLKView.m
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/29/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import "CustomGLKView.h"

@interface CustomGLKView() {
    EAGLContext *context;
    CADisplayLink *displayLink;
    CIContext *imageContext;
    CGFloat scaleFactor;
    CGRect scaledBounds;
}

-(void)setup;

@end

@implementation CustomGLKView

-(id)init {
    
    context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    self = [super initWithFrame:CGRectZero context:context];
    if (self) {
        [self setup];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    
    context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    self = [super initWithFrame:CGRectZero context:context];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    
    context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    self = [super initWithFrame:frame context:context];
    if (self) {
        [self setup];
    }
    return self;
}

-(void)setup {
    _filters = @[];
    
    scaleFactor = [[UIScreen mainScreen] scale];
    
    self.delegate = self;
    
    self.opaque = YES;
    self.userInteractionEnabled = NO;
    self.contentScaleFactor = scaleFactor;
    
    imageContext = [CIContext contextWithEAGLContext:context options:@{kCIContextWorkingColorSpace:[NSNull null]}];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    scaledBounds = CGRectMake(0.0, 0.0, ceilf(self.bounds.size.width) * scaleFactor, ceilf(self.bounds.size.height) * scaleFactor);
}

-(void)dealloc {
    [self stopRendering];
    context = nil;
    imageContext = nil;
}

-(BOOL)checkFramebufferStatus {
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        NSLog(@"Failed to create framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
        return NO;
    }
    return YES;
}

-(void)startRendering {
    displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(setNeedsDisplay)];
    displayLink.frameInterval = 1.0;
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

-(void)stopRendering {
    [displayLink invalidate];
}

-(void)render {
    [self setNeedsDisplay];
}

-(void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    CIImage *inputImage, *outputImage;
    
    if (self.sourceImage) {
        inputImage = self.sourceImage;
    } else if (self.sourceView) {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, scaleFactor);
        [self.sourceView drawViewHierarchyInRect:self.bounds afterScreenUpdates:NO];
        
        UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
        
        inputImage = [CIImage imageWithCGImage:snapshotImage.CGImage];
        
        UIGraphicsEndImageContext();
    }
    
    if (self.filters && self.filters.count > 0) {
        if (inputImage) [[self.filters objectAtIndex:0] setValue:inputImage forKey:kCIInputImageKey];
        for (int x = 0; x < self.filters.count; x++) {
            CIFilter *filter = [self.filters objectAtIndex:x];
            if (x > 0) [filter setValue:[[self.filters objectAtIndex:x-1] outputImage] forKey:kCIInputImageKey];
            if (x+1 == self.filters.count) outputImage = [filter outputImage];
        }
    } else outputImage = inputImage;
    
#warning fix?
    CGRect outputRect;
    if (!CGRectIsInfinite(outputImage.extent)) outputRect = outputImage.extent;
    else if (!CGRectIsInfinite(inputImage.extent)) outputRect = inputImage.extent;
    else outputRect = scaledBounds;
    
    [imageContext drawImage:outputImage inRect:scaledBounds fromRect:outputRect];
}

#pragma mark - Filter Management

-(CIFilter *)filterNamed:(NSString *)name {
    for (CIFilter *filter in self.filters) {
        if ([filter.name isEqualToString:name]) return filter;
    }
    return nil;
}

-(void)addFilterNamed:(NSString *)filterName withKeysAndValues:(NSDictionary *)dict {
    CIFilter *filter = [self filterNamed:filterName];
    if (filter) return;
    
    filter = [CIFilter filterWithName:filterName];
    if (!filter) return;
    
    for (NSString *key in dict) {
        [filter setValue:[dict objectForKey:key] forKey:key];
    }
    
    _filters = [_filters arrayByAddingObject:filter];
}

-(void)removeFilterNamed:(NSString *)filterName {
    CIFilter *filter = [self filterNamed:filterName];
    if (filter) {
        NSMutableArray *f = [NSMutableArray arrayWithArray:self.filters];
        [f removeObject:filter];
        _filters = [NSArray arrayWithArray:f];
    }
}

-(void)clearFilters {
    _filters = @[];
}

@end
