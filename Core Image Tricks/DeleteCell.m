//
//  DeleteCell.m
//  Core Image Tricks
//
//  Created by Tyler Tillage on 4/21/14.
//  Copyright (c) 2014 Tyler Tillage. All rights reserved.
//

#import "DeleteCell.h"
#import "CIView.h"

@implementation DeleteCell {
    CIView *overlayView;
}

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.contactImageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        self.contactImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.contactImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.contactImageView];
        
        self.nameLabel = [[UILabel alloc] init];
        self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.nameLabel.textAlignment = NSTextAlignmentCenter;
        self.nameLabel.font = [UIFont boldSystemFontOfSize:12.0];
        [self.contentView addSubview:self.nameLabel];
        
        self.phoneLabel = [[UILabel alloc] init];
        self.phoneLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.phoneLabel.textAlignment = NSTextAlignmentCenter;
        self.phoneLabel.font = [UIFont systemFontOfSize:10.0];
        [self.contentView addSubview:self.phoneLabel];
        
        [self.contentView addConstraints:[Utils
                                          constraintsForViews:@{@"image" : self.contactImageView,
                                                                @"name" : self.nameLabel,
                                                                @"phone" : self.phoneLabel}
                                          withVisualFormats:
                                          @"H:|-5-[image]-5-|",
                                          @"H:|-5-[name]-5-|",
                                          @"H:|-5-[phone]-5-|",
                                          @"V:|-5-[image(90)]-4-[name]-1-[phone]", nil]];
        
        
        overlayView = [[CIView alloc] initWithFrame:self.contentView.bounds];
        
        CIImage *mask = [CIImage imageWithCGImage:[UIImage imageNamed:@"mask.png"].CGImage];
        [overlayView addFilterNamed:@"CIColorControls" withKeysAndValues:@{@"inputSaturation" : @0.0}];
        [overlayView addFilterNamed:@"CIBlendWithMask" withKeysAndValues:@{@"inputMaskImage" : mask}];
        
        overlayView.hidden = YES;
        overlayView.sourceView = self.contentView;
        [self.contentView addSubview:overlayView];
        [self.contentView bringSubviewToFront:overlayView];
    }
    return self;
}

-(void)prepareForReuse {
    [super prepareForReuse];
    overlayView.hidden = YES;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    overlayView.frame = self.contentView.bounds;
}

-(void)animateDeletion {
    overlayView.frame = self.contentView.bounds;
    [overlayView render];
    overlayView.hidden = NO;
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteAnimationCompleted:)]) {
        [self.delegate performSelector:@selector(deleteAnimationCompleted:) withObject:self afterDelay:0.5];
    }
}

@end
